package com.inetum.aventurier.service;

import com.inetum.aventurier.model.Personnage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
@Component
public class CarteService {

    File carte = new File("src/main/resources/carte/carte.txt");
    ArrayList<String> lignesCarte = new ArrayList();

    public void getCarte() {
        try {
            Scanner sc = new Scanner(carte);
            while (sc.hasNextLine()) {
                lignesCarte.add(sc.nextLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void afficherCarteApresDeplacements(Personnage personnage) {
        List<Character> charTabX;
        char newChar = personnage.getName();
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < lignesCarte.size(); i++) {
            if (i == personnage.getPosY()) {
                charTabX = lignesCarte.get(i).chars().mapToObj(c -> (char) c).collect(Collectors.toList());
                for (int i2 = 0; i2 < charTabX.size(); i2++) {
                    if (i2 == personnage.getPosX()) {
                        charTabX.set(i2, newChar);

                        int i3 = 0;
                        while (charTabX.size() > i3) {
                            str.append(charTabX.get(i3));
                            i3++;
                        }
                    }
                }
                lignesCarte.set(personnage.getPosY(), str.toString());
            }
        }
        lignesCarte.forEach(System.out::println);
    }

    public void positionDepart(Personnage personnage) throws Exception {
        getCarte();
        if (personnage.getPosX() > 19 || personnage.getPosY() > 19) {
            throw new Exception("Hors map");
        }
        System.out.println("La position de départ est : (" + personnage.getPosX() + "," + personnage.getPosY() + ")");
        deplacer(personnage);
    }

    public void deplacer(Personnage personnage) throws Exception {

        ArrayList<String> positions = personnage.getListeCoordonnees();
        if (positions.size() <= 1) {
            throw new Exception("Pas de déplacements prévus !");
        }
        char[] positionsChar = positions.get(1).toCharArray();
        int i = 0;

        while (positionsChar.length > i) {
            switch (positionsChar[i]) {
                case 'N':
                    Integer yFinalNord = verifier(positionsChar[i], personnage.getPosX(), personnage.getPosY() - 1);
                    if (yFinalNord != null)
                        personnage.setPosY(yFinalNord);
                    break;
                case 'S':
                    Integer yFinalSud = verifier(positionsChar[i], personnage.getPosX(), personnage.getPosY() + 1);
                    if (yFinalSud != null)
                        personnage.setPosY(yFinalSud);
                    break;
                case 'O':
                    Integer xFinalOuest = verifier(positionsChar[i], personnage.getPosX() - 1, personnage.getPosY());
                    if (xFinalOuest != null)
                        personnage.setPosX(xFinalOuest);
                    break;
                case 'E':
                    Integer xFinalEst = verifier(positionsChar[i], personnage.getPosX() + 1, personnage.getPosY());
                    if (xFinalEst != null)
                        personnage.setPosX(xFinalEst);
                    break;
            }
            i++;
        }
        System.out.println("La position d'arrivée est : (" + personnage.getPosX() + "," + personnage.getPosY() + ")");
        afficherCarteApresDeplacements(personnage);
    }

    private Integer verifier(char C, int X, int Y) throws Exception {
        // verifier sur le x actuelle que la prochaine ligne Y est bien un caractere vide
        String ligneY;
        try {
            ligneY = lignesCarte.get(Y);
        } catch (Exception e) {
            throw new Exception("hors map");
        }

        List<Character> trouverX = ligneY.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        char charAVerifier = ' ';

        for (int i = 0; i < trouverX.size(); i++) {
            if (C == 'N' || C == 'S') {
                if (i == X) {
                    charAVerifier = trouverX.get(i);
                }
            }
            if (C == 'E') {
                if (i == X) {
                    charAVerifier = trouverX.get(i);
                    if (charAVerifier != '#') {
                        return X;
                    } else {
                        return X - 1;
                    }
                }
            }
            if (C == 'O') {
                if (i == X) {
                    charAVerifier = trouverX.get(i);
                    if (charAVerifier != '#') {
                        return X;
                    } else {
                        return X + 1;
                    }
                }
            }
        }
        if (C == 'N') {
            if (charAVerifier != '#') {
                return Y;
            } else {
                return Y + 1;
            }
        }
        if (C == 'S') {
            if (charAVerifier != '#') {
                return Y;
            } else {
                return Y - 1;
            }
        }
        return null;
    }
}