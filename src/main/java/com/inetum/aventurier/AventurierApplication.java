package com.inetum.aventurier;

import com.inetum.aventurier.model.Personnage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class AventurierApplication {
	public static final File test1 = new File("src/main/resources/deplacements/test1.txt");
	public static final File test2 = new File("src/main/resources/deplacements/test2.txt");

	public static void main(String[] args) throws Exception {

		SpringApplication.run(AventurierApplication.class, args);
		Personnage personnage = new Personnage('A');
		Scanner scanner = new Scanner(System.in);
		Scanner scanner2 = new Scanner(System.in);


		System.out.println("Lancement du premier test, appuyez sur une touche");
		scanner.nextLine();
		personnage.deplacements(test1);

		System.out.println("-------------------------------");

		personnage = new Personnage('A');
		System.out.println("Lancement du deuxième test, appuyez sur une touche");
		scanner2.nextLine();
		personnage.deplacements(test2);
	}
}
