package com.inetum.aventurier.model;

import com.inetum.aventurier.service.CarteService;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class Personnage {

    private final CarteService carteService = new CarteService();

    // Attributs
    private int posX;
    private int posY;
    private char name;
    private ArrayList<String> listeCoordonnees = new ArrayList<>();

    // Constructeur
    public Personnage(char name) {
        setName(name);
    }

    // Getters
    public int getPosX() {
        return this.posX;
    }

    public int getPosY() {
        return this.posY;
    }

    public char getName() {
        return this.name;
    }

    public ArrayList<String> getListeCoordonnees() {
        return this.listeCoordonnees;
    }

    // Setters

    public void setPosX(int newX) {
        this.posX = newX;
    }

    public void setPosY(int newY) {
        this.posY = newY;
    }

    public void setName(char newName) {
        this.name = newName;
    }

    public void setListeCoordonnees(ArrayList<String> newListePosition) {
        this.listeCoordonnees = newListePosition;
    }

    // Méthodes

    public ArrayList<String> getPositions(File test) throws FileNotFoundException {
        Scanner sc1 = new Scanner(test);
        ArrayList<String> listePositionFinale = new ArrayList<>();
        while (sc1.hasNextLine()) {
            listePositionFinale.add(sc1.nextLine());
        }
        this.setListeCoordonnees(listePositionFinale);
        return listePositionFinale;
    }

    public void deplacements(File test) throws Exception {
        ArrayList<String> positions = this.getPositions(test);
        String positionDepart;

        if (positions.size() == 2) {
            positionDepart = positions.get(0);
            final String SEPARATEUR = ",";
            String[] xy = positionDepart.split(SEPARATEUR);
            this.setPosX(Integer.parseInt(xy[0]));
            this.setPosY(Integer.parseInt(xy[1]));
        }
        carteService.positionDepart(this);

    }
}
